package com.mobstac.api.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.mobstac.api.database.models.Article;
import com.mobstac.api.database.models.Media;
import com.mobstac.api.database.models.Mobsite;
import com.mobstac.api.database.models.Section;
import com.mobstac.api.database.models.SubSection;
import com.mobstac.api.database.models.Tag;
import com.mobstac.api.database.models.RelatedArticles;
import com.mobstac.api.database.models.RelatedTopics;

public class MobStacSQLiteOpenHelper extends SQLiteOpenHelper {

	private static final String DATABASE_NAME = "MobStac";
	public static final String ARTICLE_TAG_JOIN_TABLE = Article.getTableName()
			+ Tag.getTableName();
	public static final String SECTION_ARTICLE_JOIN_TABLE = Section
			.getTableName() + Article.getTableName();

	private static final int DATABASE_VERSION = 44;

	private static final String CREATE_TABLE_FORMAT = "CREATE TABLE %1$s (%2$s)";

	private static final String MOBSITE_CREATE_QUERY = String.format(
			CREATE_TABLE_FORMAT, Mobsite.getTableName(),
			Mobsite.getCreateString());

	private static final String SECTION_CREATE_QUERY = String.format(
			CREATE_TABLE_FORMAT, Section.getTableName(),
			Section.getCreateString());

	private static final String SUBSECTION_CREATE_QUERY = String.format(
			CREATE_TABLE_FORMAT, SubSection.getTableName(),
			SubSection.getCreateString());

	private static final String RELATEDARTICLES_CREATE_QUERY = String.format(
			CREATE_TABLE_FORMAT, RelatedArticles.getTableName(),
			RelatedArticles.getCreateString());

	private static final String RELATEDTOPICS_CREATE_QUERY = String.format(
			CREATE_TABLE_FORMAT, RelatedTopics.getTableName(),
			RelatedTopics.getCreateString());

	private static final String TAGS_CREATE_QUERY = String.format(
			CREATE_TABLE_FORMAT, Tag.getTableName(), Tag.getCreateString());

	private static final String ARTICLE_CREATE_QUERY = String.format(
			CREATE_TABLE_FORMAT, Article.getTableName(),
			Article.getCreateString());

	private static final String MEDIA_CREATE_QUERY = String.format(
			CREATE_TABLE_FORMAT, Media.getTableName(), Media.getCreateString());

	private static final String ARTICLE_TAG_JOIN_QUERY = String.format(
			CREATE_TABLE_FORMAT,
			ARTICLE_TAG_JOIN_TABLE,
			"article_id integer, " + "tag_id integer, "
					+ "UNIQUE (tag_id, article_id) ON CONFLICT REPLACE, "
					+ "foreign key (article_id) REFERENCES "
					+ Article.getTableName() + "(_id), "
					+ "foreign key (tag_id) REFERENCES " + Tag.getTableName()
					+ "(_id)");

	private static final String SECTION_ARTICLE_JOIN_QUERY = String.format(
			CREATE_TABLE_FORMAT,
			SECTION_ARTICLE_JOIN_TABLE,
			"section_id integer, " + "article_id integer, priority integer, "
					+ "UNIQUE (section_id, article_id) ON CONFLICT REPLACE, "
					+ "foreign key (section_id) REFERENCES "
					+ Section.getTableName() + "(_id), "
					+ "foreign key (article_id) REFERENCES "
					+ Article.getTableName() + "(_id)");

	
	private static MobStacSQLiteOpenHelper mInstance;
	
	public static synchronized MobStacSQLiteOpenHelper getInstance(Context context) {
		if (mInstance == null) {
			mInstance = new MobStacSQLiteOpenHelper(context.getApplicationContext());
		}
		return mInstance;
	}

	private MobStacSQLiteOpenHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		// TODO Auto-generated method stub
		try {
			db.execSQL(MOBSITE_CREATE_QUERY);
			db.execSQL(SECTION_CREATE_QUERY);
			db.execSQL(TAGS_CREATE_QUERY);
			db.execSQL(ARTICLE_CREATE_QUERY);
			db.execSQL(SUBSECTION_CREATE_QUERY);
			db.execSQL(ARTICLE_TAG_JOIN_QUERY);
			db.execSQL(SECTION_ARTICLE_JOIN_QUERY);
			db.execSQL(MEDIA_CREATE_QUERY);
			db.execSQL(RELATEDARTICLES_CREATE_QUERY);
			db.execSQL(RELATEDTOPICS_CREATE_QUERY);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		switch(oldVersion) {
		
		case 43: {
			try {
				db.execSQL("ALTER TABLE " + Mobsite.getTableName()
					+ " ADD COLUMN pushNotification integer");
				ContentValues mobsite_values = new ContentValues();
				mobsite_values.put("pushNotification", 0);
				db.update(Mobsite.getTableName(), mobsite_values, null, null);
		    
				db.execSQL("ALTER TABLE " + Section.getTableName()
					+ " ADD COLUMN pushNotification integer");
				ContentValues section_values = new ContentValues();
				section_values.put("pushNotification", 0);
				db.update(Section.getTableName(), section_values, null, null);
			}catch (SQLException e) {
                e.printStackTrace();
            }
			break;
		}
		case 42: {
			try {
				db.execSQL("ALTER TABLE " + Mobsite.getTableName()
						+ " ADD COLUMN pushNotification integer");
				ContentValues mobsite_values = new ContentValues();
			    mobsite_values.put("pushNotification", 0);
			    db.update(Mobsite.getTableName(), mobsite_values, null, null);
			    
			    db.execSQL("ALTER TABLE " + Section.getTableName()
						+ " ADD COLUMN pushNotification integer");
				ContentValues section_values = new ContentValues();
			    section_values.put("pushNotification", 0);
			    db.update(Section.getTableName(), section_values, null, null);
			    
				db.execSQL("ALTER TABLE " + Article.getTableName()
						+ " ADD COLUMN created numeric");
				db.execSQL("UPDATE " + 
						Article.getTableName() + " SET created = updated"
						);
			}catch (SQLException e) {
                e.printStackTrace();
            }
			break;
		}
		
			case 41: {
                try {
                	db.execSQL("ALTER TABLE " + Mobsite.getTableName()
        					+ " ADD COLUMN pushNotification integer");
        			ContentValues mobsite_values = new ContentValues();
        		    mobsite_values.put("pushNotification", 0);
        		    db.update(Mobsite.getTableName(), mobsite_values, null, null);
        		    
        		    db.execSQL("ALTER TABLE " + Section.getTableName()
        					+ " ADD COLUMN pushNotification integer");
        			ContentValues section_values = new ContentValues();
        		    section_values.put("pushNotification", 0);
        		    db.update(Section.getTableName(), section_values, null, null);
        		    
				    db.execSQL("ALTER TABLE " + Article.getTableName() 
				    		+ " ADD COLUMN source text DEFAULT NULL");
				    ContentValues values = new ContentValues();
				    values.put("_id", 0);
				    values.put("name", "Home");
				    values.put("url", "/");
				    values.put("selected", 0);
				    values.put("count", 20);
				    values.put("navigation", 1);
				    values.put("sequence", -2);
				    values.put("lastSync", System.currentTimeMillis());
				    db.update(Section.getTableName(), values, "url=?", new String[] {"/"});
				    values = new ContentValues();
				    values.put("_id", 1);
				    values.put("name", "Saved Articles");
				    values.put("url", "/saved/");
				    values.put("selected", 0);
				    values.put("count", 20);
				    values.put("navigation", 1);
				    values.put("sequence", -1);
				    values.put("lastSync", System.currentTimeMillis());
				    db.insert(Section.getTableName(), null, values);
                }
                catch (SQLException e) {
                    e.printStackTrace();
                }
				break;
			}
			default: {
                try {
                	db.execSQL("ALTER TABLE " + Mobsite.getTableName()
        					+ " ADD COLUMN pushNotification integer");
        			ContentValues mobsite_values = new ContentValues();
        		    mobsite_values.put("pushNotification", 0);
        		    db.update(Mobsite.getTableName(), mobsite_values, null, null);
        		    
        		    db.execSQL("ALTER TABLE " + Section.getTableName()
        					+ " ADD COLUMN pushNotification integer");
        			ContentValues section_values = new ContentValues();
        		    section_values.put("pushNotification", 0);
        		    db.update(Section.getTableName(), section_values, null, null);
        		    
				    db.execSQL("ALTER TABLE " + Mobsite.getTableName() 
				    		+ " ADD COLUMN lastSync integer DEFAULT '0'");
				    db.execSQL("ALTER TABLE " + Article.getTableName() 
				    		+ " ADD COLUMN source text DEFAULT NULL");
				    db.execSQL("ALTER TABLE " + Article.getTableName()
							+ " ADD COLUMN created numeric");
				    db.execSQL("UPDATE " + 
							Article.getTableName() + " SET created = updated"
							);
                }
                catch (SQLException e) {
                    e.printStackTrace();
                }
				break;
			}
		}
	}

}
