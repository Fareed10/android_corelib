package com.mobstac.api.database.models;

public class RelatedArticles extends ModelBase {
	private final static String tableName = "RelatedArticles";

	private final static String all_columns[] = { "_id", "content_id", "title", "url" };

	private final static String all_RelatedArticles_columns[] = { "RelatedArticles._id as _id", "RelatedArticles.content_id as content_id",
			"RelatedArticles.title as title", "RelatedArticles.url as url" };

	private final static String column_types[] = { "integer primary key autoincrement", "integer",
			"text", "text," + "UNIQUE (content_id, url) ON CONFLICT REPLACE," +
					" foreign key (content_id) REFERENCES "
					+ Article.getTableName() + "(_id)"};

	public static String[] getAllColumns() {
		return all_columns;
	}

	public static String[] getAllRelatedArticlesColumns() {
		return all_RelatedArticles_columns;
	}

	public static String[] getUrls() {
		return null;

	}

	public static String getCreateString() {
		StringBuilder query = new StringBuilder();
		for (int i = 0; i < all_columns.length; i++) {
			if (i != 0) {
				query.append(", ");
			}
			query.append(all_columns[i] + " " + column_types[i]);
		}
		return query.toString();
	}

	public static String getTableName() {
		return tableName;
	}


}
