package com.mobstac.api.database.models;

public class RelatedTopics extends ModelBase {
	private final static String tableName = "RelatedTopics";

	private final static String all_columns[] = { "_id", "content_id", "title", "url" };

	private final static String all_RelatedTopics_columns[] = { "RelatedTopics._id as _id", "RelatedTopics.content_id as content_id",
			"RelatedTopics.title as title", "RelatedTopics.url as url" };

	private final static String column_types[] = { "integer primary key autoincrement", "integer",
			"text", "text," + "foreign key (content_id) REFERENCES "
					+ Article.getTableName() + "(_id)"};

	public static String[] getAllColumns() {
		return all_columns;
	}

	public static String[] getAllRelatedTopicsColumns() {
		return all_RelatedTopics_columns;
	}

	public static String[] getUrls() {
		return null;

	}

	public static String getCreateString() {
		StringBuilder query = new StringBuilder();
		for (int i = 0; i < all_columns.length; i++) {
			if (i != 0) {
				query.append(", ");
			}
			query.append(all_columns[i] + " " + column_types[i]);
		}
		return query.toString();
	}

	public static String getTableName() {
		return tableName;
	}



}
