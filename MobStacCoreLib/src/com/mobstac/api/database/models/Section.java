package com.mobstac.api.database.models;

public class Section extends ModelBase {

	public static String api_key = "/sections/";

	private final static String tableName = "Section";

	private final static String all_columns[] = { "_id", "name", "url",
			"selected", "count", "navigation", "sequence", "lastSync", "subsection_count", "pushNotification" };
	private final static String all_named_columns[] = { "Section._id as _id",
			"Section.name as name", "Section.url as url",
			"Section.selected as selected", "Section.count as count",
			"Section.navigation as navigation", "Section.sequence as sequence",
			"Section.lastSync as lastSync", "Section.subsection_count as subsection_count", "Section.pushNotification as pushNotification" };
	private final static String column_types[] = { "integer primary key",
			"text", "text unique", "integer", "integer", "integer", "integer", "integer", "integer", "integer" };

	public static String[] getAllColumns() {
		return all_columns;
	}

	public static String[] getAllNamedColumns() {
		return all_named_columns;
	}

	public static String[] getUrls() {
		return new String[] { api_key };
	}

	public static String getCreateString() {
		StringBuilder query = new StringBuilder();
		for (int i = 0; i < all_columns.length; i++) {
			if (i != 0) {
				query.append(", ");
			}
			query.append(all_columns[i] + " " + column_types[i]);
		}
		return query.toString();
	}

	public static String getTableName() {
		return tableName;
	}

}
