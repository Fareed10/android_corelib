package com.mobstac.api.database.models;

public class Article extends ModelBase {

	private final static String tableName = "Article";

	private final static String all_columns[] = { "_id", "title", "url",
			"summary", "updated", "content", "starred", "author", "source",
			"comment_count", "mobstac_id", "created" };

	private final static String all_article_columns[] = { "Article._id as _id",
			"Article.title as title", "Article.url as url",
			"Article.summary as summary", "Article.updated as updated",
			"Article.content as content", "Article.starred as starred",
			"Article.author as author", "Article.source as source",
			"Article.comment_count as comment_count",
			"Article.mobstac_id as mobstac_id", "Article.created as created" };

	private final static String column_types[] = { "integer primary key",
			"text", "text unique", "blob", "numeric", "blob", "integer",
			"text", "text", "integer", "integer", "numeric" };

	public static String[] getAllColumns() {
		return all_columns;
	}

	public static String[] getAllArticleColumns() {
		return all_article_columns;
	}

	public static String[] getUrls() {
		return null;

	}

	public static String getCreateString() {
		StringBuilder query = new StringBuilder();
		for (int i = 0; i < all_columns.length; i++) {
			if (i != 0) {
				query.append(", ");
			}
			query.append(all_columns[i] + " " + column_types[i]);
		}
		return query.toString();
	}

	public static String getTableName() {
		return tableName;
	}

}
