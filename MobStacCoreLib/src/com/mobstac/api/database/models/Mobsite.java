package com.mobstac.api.database.models;

public class Mobsite extends ModelBase {

	public static String api_key = "/";

	private final static String tableName = "Mobsite";

	private final static String all_columns[] = { "_id", "name", "publisherTLD", "lastSync", "pushNotification" };
	private final static String column_types[] = { "integer primary key",
			"text unique", "text", "integer", "integer" };

	public static String[] getAllColumns() {
		return all_columns;
	}

	public static String getCreateString() {
		StringBuilder query = new StringBuilder();
		for (int i = 0; i < all_columns.length; i++) {
			if (i != 0) {
				query.append(", ");
			}
			query.append(all_columns[i] + " " + column_types[i]);
		}
		return query.toString();
	}

	public static String[] getUrls() {
		return new String[] { api_key };
	}

	public static String getTableName() {
		return tableName;
	}

}
