package com.mobstac.api.database.models;

public abstract class ModelBase {

	protected final static String idString = "_id";

	protected final static String[] base_columns = { idString };

}
