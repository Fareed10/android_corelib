package com.mobstac.api.database.models;

public class Media extends ModelBase {

	private final static String tableName = "Media";

	private final static String all_columns[] = { "_id", "article_id", "url",
			"width", "height", "type", "altText", "sequence" };

	private final static String all_media_columns[] = {
			"Media._id as media_id", "Media.article_id", "Media.url as media_url",
			"Media.width", "Media.height", "Media.type", "Media.altText" };

	private final static String column_types[] = {
			"integer primary key autoincrement",
			"integer",
			"text",
			"integer",
			"integer",
			"text",
			"text", 
			"integer,"+"foreign key (article_id) REFERENCES "
					+ Article.getTableName() + "(_id) UNIQUE (article_id, url)" };

	public static String[] getAllColumns() {
		return all_columns;
	}

	public static String[] getAllMediaColumns() {
		return all_media_columns;
	}

	public static String[] getUrls() {
		return null;
	}

	public static String getCreateString() {
		StringBuilder query = new StringBuilder();
		for (int i = 0; i < all_columns.length; i++) {
			if (i != 0) {
				query.append(", ");
			}
			query.append(all_columns[i] + " " + column_types[i]);
		}
		return query.toString();
	}

	public static String getTableName() {
		return tableName;
	}

}
