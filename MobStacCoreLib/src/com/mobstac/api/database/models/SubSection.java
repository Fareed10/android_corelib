package com.mobstac.api.database.models;

public class SubSection extends ModelBase {

	private final static String tableName = "SubSection";

	private final static String all_columns[] = { "_id", "section_id",
			"article_count", "show_title", "has_lead", "sequence", "enabled", "group_id" };

	private final static String all_subsection_columns[] = {
			"SubSection._id as subsection_id",
			"SubSection.section_id as section_id",
			"SubSection.article_count as article_count",
			"SubSection.show_title as show_title",
			"SubSection.has_lead as has_lead",
			"SubSection.sequence as sub_sequence," +
			"SubSection.enabled as enabled",
			"SubSection.group_id as group_id"};

	private final static String column_types[] = {
			"integer",
			"integer",
			"integer",
			"integer",
			"integer",
			"integer",
			"integer",
			"integer, UNIQUE (section_id, _id) ON CONFLICT REPLACE, "
					+ "foreign key (section_id) REFERENCES "
					+ Section.getTableName() + "(_id), "
					+ "foreign key (_id) REFERENCES " + Section.getTableName()
					+ "(_id)" };

	public static String[] getAllColumns() {
		return all_columns;
	}

	public static String[] getAllSubSectionColumns() {
		return all_subsection_columns;
	}

	public static String[] getUrls() {
		return null;

	}

	public static String getCreateString() {
		StringBuilder query = new StringBuilder();
		for (int i = 0; i < all_columns.length; i++) {
			if (i != 0) {
				query.append(", ");
			}
			query.append(all_columns[i] + " " + column_types[i]);
		}
		return query.toString();
	}

	public static String getTableName() {
		return tableName;
	}

}
