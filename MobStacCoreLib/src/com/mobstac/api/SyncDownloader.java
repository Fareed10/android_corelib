package com.mobstac.api;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLDecoder;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import org.apache.http.Header;
import org.apache.http.HeaderElement;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.content.ContentProviderClient;
import android.content.ContentValues;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.res.Resources;
import android.content.res.Resources.NotFoundException;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.RemoteException;
import android.text.Html;
import android.util.Log;
import com.mobstac.api.database.MobStacSQLiteOpenHelper;
import com.mobstac.api.database.models.Article;
import com.mobstac.api.database.models.Media;
import com.mobstac.api.database.models.Mobsite;
import com.mobstac.api.database.models.RelatedArticles;
import com.mobstac.api.database.models.Section;
import com.mobstac.api.database.models.SubSection;
import com.mobstac.api.util.MobStacProfiler;

/**
 * {@link SyncDownloader} is a utility class that facilitates connection to
 * MobStac API. The major use for {@link SyncDownloader} is that it initializes
 * the first use scenario for the app. In order to connect to api in test mode,
 * please use android:isdebuggable in your manifest file.
 */
@SuppressLint("SimpleDateFormat")
public class SyncDownloader {

	private Mac mac;

	private Context mContext;
	private DownloadListener updateCallback;

	private static Object lock;
	private static Object mobsiteSyncLock;
	private String apiKey;
	private String secretKey;

	private int mobsiteId;

	static {
		lock = new Object();
		mobsiteSyncLock = new Object();
	}

	/**
	 * {@link DownloadListener} allows SyncDownloader to provide feedback to the
	 * user in case network is disconnected or any other errors.
	 */
	public interface DownloadListener {
		public void update(String update);

		public void finished();
	}

	/**
	 * @param context
	 *            The context that SyncDownloader can use to read strings file
	 *            for API keys.
	 * @param updateCallback
	 *            listener to publish updates to user.
	 */
	public SyncDownloader(Context context, DownloadListener updateCallback) {
		mContext = context;
		this.updateCallback = updateCallback;
		this.mobsiteId = mContext.getResources().getInteger(
				MobStacProvider.getResourseIdByName(mContext.getPackageName(),
						"integer", "mobsite_id"));
		apiKey = mContext
				.getApplicationContext()
				.getResources()
				.getString(
						MobStacProvider.getResourseIdByName(
								mContext.getPackageName(), "string", "api_key"));
		secretKey = mContext
				.getApplicationContext()
				.getResources()
				.getString(
						MobStacProvider.getResourseIdByName(
								mContext.getPackageName(), "string",
								"secret_key"));
		assert (apiKey != null && apiKey.length() != 0);
		assert (secretKey != null && apiKey.length() != 0);

		try {
			mac = Mac.getInstance("HmacSHA256");
			SecretKeySpec secret = new SecretKeySpec(
					secretKey.getBytes("UTF-8"), mac.getAlgorithm());
			mac.init(secret);
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvalidKeyException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private static String bytesToHex(byte[] bytes) {
		final char[] hexArray = { '0', '1', '2', '3', '4', '5', '6', '7', '8',
				'9', 'A', 'B', 'C', 'D', 'E', 'F' };
		char[] hexChars = new char[bytes.length * 2];
		int v;
		for (int j = 0; j < bytes.length; j++) {
			v = bytes[j] & 0xFF;
			hexChars[j * 2] = hexArray[v >>> 4];
			hexChars[j * 2 + 1] = hexArray[v & 0x0F];
		}
		return new String(hexChars);
	}

	private JSONObject downloadData(String url) {
		HttpURLConnection urlConnection;

		try {
			byte[] digest = mac.doFinal(URLDecoder.decode(url, "utf-8")
					.getBytes());
			String retVal = bytesToHex(digest).toLowerCase(Locale.US);

			boolean isDebug = (0 != (mContext.getApplicationInfo().flags &= ApplicationInfo.FLAG_DEBUGGABLE));

			if (isDebug) {
				url = "https://api.qa.mobstac.com"
						+ url.concat("&signature=" + retVal);
			} else {
				url = "https://api.mobstac.com"
						+ url.concat("&signature=" + retVal);
			}
			Log.v("Download Starting", url);
			MobStacProfiler.getProfiler(url).startProfiling();
			URL urlHandler = new URL(url);
			urlConnection = (HttpURLConnection) urlHandler.openConnection();
			//urlConnection.setRequestProperty("Connection","close");
			urlConnection.setConnectTimeout(30000);
			InputStream in = new BufferedInputStream(
					urlConnection.getInputStream());
			BufferedReader reader = new BufferedReader(
					new InputStreamReader(in));
			StringBuilder builder = new StringBuilder();
			String line;
			while ((line = reader.readLine()) != null) {
				builder.append(line);
			}
			MobStacProfiler.getProfiler(url).stopProfiling();
			JSONObject jsonObj = new JSONObject(builder.toString());
			urlConnection.disconnect();
			in.close();
			return jsonObj;
		}catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (OutOfMemoryError e) {
			// TODO: handle exception
			e.printStackTrace();
		}finally {
			urlConnection = null;
		}
		return null;
	}

	/**
	 * This method should be called everytime app starts to make sure app has
	 * the latest data for all sections in the mobsite.
	 * 
	 * @return status of the sync operation, if the app is being loaded for the
	 *         first time and sync operation fails there will be no data for app
	 *         to show to the user.
	 */
	public boolean syncMobsite() {
		synchronized (mobsiteSyncLock) {
			String api_url = mobsiteId + "/";
			MobStacSQLiteOpenHelper database = MobStacSQLiteOpenHelper
					.getInstance(mContext);
			SQLiteDatabase db = database.getWritableDatabase();
			Cursor existingData = db.query(Mobsite.getTableName(),
					Mobsite.getAllColumns(), "_id=?", new String[] { mobsiteId
				+ "" }, null, null, null);

			if (existingData.moveToFirst()) {
				long lastSync = existingData.getLong(existingData
						.getColumnIndex("lastSync"));
				if (lastSync > System.currentTimeMillis() - 24 * 60 * 60000) {
					existingData.close();
					return true;
				}
			}
			existingData.close();
			if (updateCallback != null) {
				updateCallback.update("Fetching settings");
			}
			boolean success = true;
			db.beginTransaction();
			JSONObject data = callAPI(api_url, null, "GET");
			if (data != null) {
				ContentValues values = new ContentValues();
				values.put("_id", mobsiteId);
				try {
					db.delete(Mobsite.getTableName(), "_id=?",
							new String[] { "" + mobsiteId });
					values.put("name", data.getString("name"));
					values.put("publisherTLD", data.getString("orig_domain"));
					values.put("lastSync", System.currentTimeMillis());
					if (data.optBoolean("pushNotification") == true)
						values.put("pushNotification", 1);
					else
						values.put("pushNotification", 0);
					db.insert(Mobsite.getTableName(), null, values);
					Cursor homeSubsections = db.query(SubSection.getTableName(), new String[]{"_id","enabled"}, "section_id=?", new String[] {"0"}, null, null, null);
					Map<Integer,Boolean> subsectionIds = new HashMap<Integer,Boolean>();
					if(homeSubsections != null){
						if(homeSubsections.getCount() > 0){
							homeSubsections.moveToFirst();
							do
							{
								Integer subsectionId = homeSubsections.getInt(homeSubsections.getColumnIndex("_id"));
								Integer subsectionEnabled = homeSubsections.getInt(homeSubsections.getColumnIndex("enabled"));
								boolean subsectionShowOnHome;
								if(subsectionEnabled == 1)
									subsectionShowOnHome = true;
								else
									subsectionShowOnHome = false;
								subsectionIds.put(subsectionId, subsectionShowOnHome);
							}while(homeSubsections.moveToNext());
						}
						homeSubsections.close();
					}
					db.delete(Section.getTableName(), null, null);
					db.delete(SubSection.getTableName(), null, null);
					values = new ContentValues();
					values.put("_id", 0);
					values.put("name", "Home");
					values.put("url", "/");
					values.put("selected", 0);
					values.put("count", 20);
					values.put("navigation", 1);
					values.put("sequence", -2);
					values.put("lastSync", System.currentTimeMillis());
					values.put("pushNotification", 0);
					db.insert(Section.getTableName(), null, values);
					values = new ContentValues();
					values.put("_id", 1);
					values.put("name", "Saved Articles");
					values.put("url", "/saved/");
					values.put("selected", 0);
					values.put("count", 20);
					values.put("navigation", 1);
					values.put("sequence", -1);
					values.put("lastSync", System.currentTimeMillis());
					values.put("pushNotification", 0);
					db.insert(Section.getTableName(), null, values);
					boolean retVal = syncSections(db);
					if (retVal) {
						ContentValues subsectValues = new ContentValues();
						for (Integer subsectionId : subsectionIds.keySet()) {
							subsectValues.put("enabled", subsectionIds.get(subsectionId));
							db.update(SubSection.getTableName(), subsectValues, "_id=?", new String[] {"" + subsectionId});
						}
						db.setTransactionSuccessful();
					} else {
						success = false;
					}
				} catch (JSONException e) {
					e.printStackTrace();
					success = false;
				} catch (NotFoundException e) {
					e.printStackTrace();
					success = false;
				}
			} else {
				if (updateCallback != null) {
					updateCallback.update("No network connection detected");
				}
			}
			db.endTransaction();
			return success;
		}
	}

	private boolean syncSections(SQLiteDatabase db) {
		int current = 0;
		int total = 1;
		while (current < total) {
			String api_url = mobsiteId + "/sections/";
			Map<String, String> params = new HashMap<String, String>();
			params.put("start", current + "");
			JSONObject sections = callAPI(api_url, params, "GET");
			if (sections != null) {
				try {
					total = sections.getInt("total");
					JSONArray sectionList = sections.getJSONArray("sections");
					for (int i = 0; i < sectionList.length(); i++) {
						syncSection(sectionList.getJSONObject(i), db);
					}
					current += sectionList.length();
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (RemoteException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} else {
				current = total;
				if (updateCallback != null)
					updateCallback.update("No network connection detected");
				return false;
			}
		}
		return true;
	}

	private void syncSection(JSONObject sectionObj, SQLiteDatabase db)
			throws JSONException, RemoteException {
		boolean showOnNav = false;
		boolean showOnHomePage = false;
		boolean showTitleOnHome = false;
		if (sectionObj.getJSONObject("display_options").optBoolean(
				"showOnAppNav"))
			showOnNav = true;
		if (sectionObj.getJSONObject("display_options").optBoolean(
				"showOnAppHomePage"))
			showOnHomePage = true;
		if (sectionObj.getJSONObject("display_options").optBoolean(
				"showTitleOnAppHome"))
			showTitleOnHome = true;
		ContentValues values = new ContentValues();
		values.put("_id", sectionObj.getInt("id"));
		if (sectionObj.getString("name").equalsIgnoreCase("cities")) {
			values.put("name", "City News");
		} else {
			values.put("name", sectionObj.getString("name"));
		}
		if (updateCallback != null) {
			updateCallback.update("Fetching data for section: "
					+ values.getAsString("name"));
		}
		values.put("url", sectionObj.getString("url"));
		values.put("lastSync", 0);
		if (sectionObj.optBoolean("pushNotification") == true)
			values.put("pushNotification", 1);
		else
			values.put("pushNotification", 0);
		JSONArray subSections = sectionObj.getJSONArray("app_sub_sections");
		values.put("subsection_count", subSections.length());
		int sectionId = sectionObj.getInt("id");
		if ((showOnHomePage && showTitleOnHome) || showOnNav) {
			values.put("selected", 1);
		} else {
			values.put("selected", 0);
		}
		if (showOnNav) {
			values.put("navigation", 1);
		} else {
			values.put("navigation", 0);
		}
		values.put("sequence", sectionObj.getJSONObject("display_options")
				.optInt("dropDownSeqNum", 100));
		values.put(
				"count",
				sectionObj.getJSONObject("display_options").optInt(
						"app_articles_to_show", 5));

		db.insert(Section.getTableName(), null, values);

		if (showOnHomePage || showOnNav) {
			ContentValues subSectionValues = new ContentValues();
			subSectionValues.put("_id", sectionId);
			subSectionValues.put("section_id", 0);
			subSectionValues.put(
					"article_count",
					sectionObj.getJSONObject("display_options").optInt(
							"articlesOnAppHomePage", 2));
			subSectionValues.put(
					"show_title",
					sectionObj.getJSONObject("display_options").optBoolean(
							"showTitleOnAppHome", true));
			subSectionValues.put(
					"has_lead",
					sectionObj.getJSONObject("display_options").optBoolean(
							"showLeadOnAppHome", false));
			subSectionValues.put(
					"sequence",
					sectionObj.getJSONObject("display_options").optInt(
							"appHomePageSequence", 100));
			if (showOnHomePage) {
				subSectionValues.put("enabled", true);
			} else {
				subSectionValues.put("enabled", false);
			}
			subSectionValues.put(
					"group_id",
					sectionObj.getJSONObject("display_options").optInt(
							"appHomeSectionGroup", sectionId));
			db.insert(SubSection.getTableName(), null, subSectionValues);
		}

		for (int j = 0; j < subSections.length(); j++) {
			JSONObject subSectionObj = subSections.getJSONObject(j);
			ContentValues subSectionValues = new ContentValues();
			subSectionValues.put("_id", subSectionObj.getInt("id"));
			subSectionValues.put("section_id", sectionObj.getInt("id"));
			subSectionValues.put("article_count",
					subSectionObj.optInt("articles", 5));
			subSectionValues.put("show_title",
					subSectionObj.optBoolean("displayTitle"));
			subSectionValues.put("has_lead",
					subSectionObj.optBoolean("showLead"));
			subSectionValues.put("sequence", j);
			subSectionValues.put("enabled", true);
			subSectionValues.put("group_id", subSectionObj.getInt("id"));
			db.insert(SubSection.getTableName(), null, subSectionValues);
		}

	}

	Cursor syncArticle(String title, String url) {
		String api_url = mobsiteId + "/contents/";
		Map<String, String> params = new HashMap<String, String>();
		params.put("path", url);
		JSONObject downloadData = callAPI(api_url, params, "GET");
		if (downloadData == null) {
			return null;
		}
		String[] columns = new String[] { "_id", "title", "author", "url",
				"summary", "updated", "content", "source", "created" };
		MatrixCursor cursor = new MatrixCursor(columns);
		JSONArray contentArray = downloadData.optJSONArray("content");
		for (int i = 0; contentArray != null && i < contentArray.length(); i++) {
			JSONObject content = contentArray.optJSONObject(i);
			if (content != null && content.optString("link", "").equals(url)) {
				MatrixCursor.RowBuilder row = cursor.newRow();
				try {
					row.add(content.getInt("id"));
					row.add(content.getString("title"));
					row.add(content.optString("author", ""));
					row.add(content.getString("link"));
					row.add(content.getJSONObject("body").optString("summary"));
					SimpleDateFormat dateFormat = new SimpleDateFormat(
							"yyyy-MM-dd HH:mm:ss");
					Date created_date, updated_date;
					created_date = dateFormat.parse(content.getJSONObject("body")
							.optString("orig_publish_date", content.getString("updated")));
					updated_date = dateFormat.parse(content.getJSONObject("body")
							.optString("orig_updated_time", content.getString("updated")));
					row.add(updated_date.getTime() - updated_date.getTimezoneOffset() * 60
							* 1000);

					String fullText = Pattern
							.compile("\\{([^\\}]*)\\}")
							.matcher(
									content.getJSONObject("body").optString(
											"full")).replaceAll("");
					row.add(fullText);
					row.add(content.optString("source"));
					row.add(created_date.getTime() - created_date.getTimezoneOffset() * 60 * 1000);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return cursor;
	}

	boolean syncArticles(int sectionId, boolean forced, String count) {
		MobStacSQLiteOpenHelper database = MobStacSQLiteOpenHelper
				.getInstance(mContext);
		SQLiteDatabase db = database.getReadableDatabase();
		String api_url = mobsiteId + "/sections/" + sectionId + "/settings/";
		Cursor cursor = db.query(Section.getTableName(),
				Section.getAllColumns(), "_id=?",
				new String[] { sectionId + "" }, null, null, null);
		long currentTime = System.currentTimeMillis();
		boolean success = true;
		db = null;
		while (cursor.moveToNext()) {
			long lastSyncTime = 0;
			lastSyncTime = cursor.getLong(cursor.getColumnIndex("lastSync"));

			if (!forced && (currentTime - lastSyncTime) < 15 * 60 * 1000) {
				cursor.close();
				return success;
			}

			if (updateCallback != null) {
				updateCallback.update("Fetching articles for section: "
						+ cursor.getString(cursor.getColumnIndex("name")));
			}
			api_url = mobsiteId + "/sections/"
					+ cursor.getInt(cursor.getColumnIndex("_id"))
					+ "/contents/";
			Map<String, String> params = new HashMap<String, String>();
			params.put("count", count);
			JSONObject data = callAPI(api_url, params, "GET");
			synchronized (lock) {
				SQLiteDatabase writeDb = database.getWritableDatabase();
				writeDb.beginTransaction();
				writeDb.delete(
						MobStacSQLiteOpenHelper.SECTION_ARTICLE_JOIN_TABLE,
						"section_id=?",
						new String[] { cursor.getInt(cursor
								.getColumnIndex("_id")) + "" });
				JSONArray articleList = null;
				int total = 0;
				if (data != null) {
					articleList = data.optJSONArray("content");
					total = articleList.length();
				} else {
					success = false;
				}

				for (int i = 0; articleList != null && i < articleList.length(); i++) {
					try {
						ContentValues values = new ContentValues();
						JSONObject articleObj = articleList.optJSONObject(i);
						String id = null;
						if (mobsiteId == 2550 || mobsiteId == 3696) { //Custom logic for Hindu and HBL
							Pattern p = Pattern.compile("article(\\d+)");
							Matcher m = p.matcher(articleObj.getString("link"));
							if (m.find()) {
								id = m.group(1);
							}
						}
						else { //Default case
							id = articleObj.getString("id");
						}
						Cursor existingData = writeDb.query(
								Article.getTableName(),
								Article.getAllColumns(), "_id=?",
								new String[] { id }, null, null, null);
						values.put("_id", id);
						values.put("title", articleObj.getString("title"));
						values.put("url", articleObj.getString("link"));
						values.put("author", articleObj.getString("author"));
						values.put("source", articleObj.optString("source"));
						values.put("summary", articleObj.getJSONObject("body")
								.getString("summary"));
						values.put("mobstac_id", articleObj.getInt("id"));
						values.put("comment_count",
								articleObj.getInt("comment_count"));
						values.put("priority", currentTime + total - i);

						SimpleDateFormat dateFormat = new SimpleDateFormat(
								"yyyy-MM-dd HH:mm:ss");
						Date created_date, updated_date;
						created_date = dateFormat.parse(articleObj.getJSONObject("body")
								.optString("orig_publish_date", articleObj.getString("updated")));
						updated_date = dateFormat.parse(articleObj.getJSONObject("body")
								.optString("orig_updated_time", articleObj.getString("updated")));
						//						if(created_date.toString() == null)
						//							created_date = dateFormat.parse(articleObj.getString("updated"));
						//						if(updated_date.toString() == null)
						//							updated_date = dateFormat.parse(articleObj.getString("updated"));
						values.put("created",
								created_date.getTime() - created_date.getTimezoneOffset() * 60
								* 1000);
						values.put("updated",
								updated_date.getTime() - updated_date.getTimezoneOffset() * 60
								* 1000);
						String fullText = Pattern
								.compile("\\{([^\\}]*)\\}")
								.matcher(
										articleObj.getJSONObject("body")
										.getString("full"))
										.replaceAll("");

						values.put("content", fullText);
						if (existingData.moveToFirst()) {
							long priority = Long.MIN_VALUE;
							Cursor priorityData = writeDb
									.query(MobStacSQLiteOpenHelper.SECTION_ARTICLE_JOIN_TABLE,
											new String[] { "priority" },
											"section_id=? AND article_id=?",
											new String[] { sectionId + "", id },
											null, null, null);
							if (priorityData.getCount() > 0) {
								priorityData.moveToFirst();
								priority = priorityData.getLong(priorityData.getColumnIndex("priority"));
							}
							if (priority <= values.getAsLong("priority")) {
								articleUpdate(writeDb, values,
										Integer.parseInt(id),
										cursor.getInt(cursor
												.getColumnIndex("_id")));
							}
							priorityData.close();
						} else {
							articleInsert(writeDb, values,
									Integer.parseInt(id), sectionId);
						}
						existingData.close();
						if (articleObj.getJSONObject("body").optString(
								"video_script", null) != null) {
							ContentValues imageValue = new ContentValues();
							imageValue.put(
									"url",
									articleObj.getJSONObject("body").getString(
											"video_script"));
							imageValue.put("width", 200);
							imageValue.put("height", 200);
							imageValue.put("type", "video_script");
							imageValue.put("sequence", 0);
							imageValue.put("article_id", Integer.parseInt(id));
							imageValue.put("altText", "");
							Cursor existingImage = writeDb.query(Media
									.getTableName(),
									Media.getAllMediaColumns(), "Media.url=?",
									new String[] { imageValue
								.getAsString("url") }, null, null,
								null);
							if (existingImage.getCount() == 0)
								writeDb.insert(Media.getTableName(), null,
										imageValue);
							existingImage.close();
						}
						JSONArray images = articleObj.getJSONObject("media")
								.getJSONArray("images");
						for (int j = 0; j < images.length(); j++) {
							JSONObject image = images.getJSONObject(j);
							ContentValues imageValue = new ContentValues();
							Uri.Builder builder = new Uri.Builder();
							builder.scheme("http")
							.authority("cdn.mobstac.com")
							.appendPath("m")
							.appendPath("img")
							.appendPath("")
							.appendQueryParameter("src",
									image.getString("origSrc"))
									.appendQueryParameter("w", "origWidth");
							imageValue.put("url", builder.build().toString());
							imageValue.put("width", image.getInt("width"));
							imageValue.put("height", image.getInt("height"));
							imageValue.put("type", "image");
							imageValue.put("sequence", j + 1);
							imageValue.put("article_id", Integer.parseInt(id));
							imageValue.put("altText",
									image.getString("altText"));
							Cursor existingImage = writeDb.query(Media
									.getTableName(),
									Media.getAllMediaColumns(), "Media.url=? AND Media.article_id=?",
									new String[] { imageValue
								.getAsString("url"), imageValue.getAsString("article_id") }, null, null,
								null);
							if (existingImage.getCount() == 0)
								writeDb.insert(Media.getTableName(), null,
										imageValue);
							existingImage.close();
						}
                        
                        if (mobsiteId == 25308){ 			//related videos list only for deccanherald
                            if(sectionId == 13472){
                                JSONArray related_videos_list = articleObj.getJSONObject("media")
                                .getJSONArray("related_videos");
                                int videos_len = related_videos_list.length();
                                for (int j = 0; j < videos_len; j++) {
                                    JSONObject video = related_videos_list.getJSONObject(j);
                                    ContentValues videoValue = new ContentValues();
                                    Uri.Builder builder = new Uri.Builder();
                                    builder.scheme("http")
                                    .authority("cdn.mobstac.com")
                                    .appendPath("m")
                                    .appendPath("img")
                                    .appendPath("")
                                    .appendQueryParameter("src",
                                                          video.getString("imgSrc"))
                                    .appendQueryParameter("w", "origWidth");
                                    videoValue.put("url", builder.build().toString());
                                    videoValue.put("type", "video");
                                    videoValue.put("sequence", j + 1);
                                    videoValue.put("article_id", Integer.parseInt(id));
                                    videoValue.put("altText",
                                                   video.getString("title") + "&" + video.getString("href"));
                                    Cursor existingVideo = writeDb.query(Media
                                                                         .getTableName(),
                                                                         Media.getAllMediaColumns(), "Media.url=? AND Media.article_id=?",
                                                                         new String[] { videoValue
                                                                             .getAsString("url"), videoValue.getAsString("article_id") }, null, null,
                                                                         null);
                                    if (existingVideo.getCount() == 0){
                                        writeDb.insert(Media.getTableName(), null,
                                                       videoValue);
                                        Log.v("Inserted Video", videoValue.toString());
                                    }
                                    existingVideo.close();
                                }
                            }
                        }
                        
						JSONArray relatedArticles = articleObj.getJSONObject(
								"body").optJSONArray("relatedArticles");
						for (int j = 0; relatedArticles != null
								&& j < relatedArticles.length(); j++) {
							JSONArray rArticle = relatedArticles
									.getJSONArray(j);
							ContentValues value = new ContentValues();
							value.put("title", rArticle.getString(0));
							value.put("url", rArticle.getString(1));
							value.put("content_id", Integer.parseInt(id));
							Cursor existingRArticle = writeDb
									.query(RelatedArticles.getTableName(),
											RelatedArticles
											.getAllRelatedArticlesColumns(),
											"RelatedArticles.url=? AND RelatedArticles.content_id=?",
											new String[] {
										value.getAsString("url"),
										id }, null, null, null);
							if (existingRArticle.getCount() == 0)
								writeDb.insert(RelatedArticles.getTableName(),
										null, value);
							existingRArticle.close();

						}
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				ContentValues values = new ContentValues();
				values.put("lastSync", currentTime);
				writeDb.update(Section.getTableName(), values, "_id=?",
						new String[] { sectionId + "" });
				if (success) {
					writeDb.setTransactionSuccessful();
				}
				writeDb.endTransaction();
			}
		}
		cursor.close();
		return success;
	}

	private int articleUpdate(SQLiteDatabase db, ContentValues values,
			int articleId, int sectionId) {
		long priority = 0;
		if (values.containsKey("priority")) {
			priority = values.getAsLong("priority");
			values.remove("priority");
		}
		int affected = db.update(Article.getTableName(), values, "_id=?",
				new String[] { articleId + "" });
		if (sectionId != -1) {
			ContentValues joinValues = new ContentValues();
			joinValues.put("article_id", articleId);
			joinValues.put("section_id", sectionId);
			if (priority != 0)
				joinValues.put("priority", priority);
			db.insert(MobStacSQLiteOpenHelper.SECTION_ARTICLE_JOIN_TABLE, null,
					joinValues);
		}

		return affected;
	}

	private void articleInsert(SQLiteDatabase db, ContentValues values,
			int articleId, int sectionId) {
		long priority = values.getAsLong("priority");
		values.remove("priority");
		try {
			db.insert(Article.getTableName(), null, values);
		} catch (SQLiteConstraintException e) {
			e.printStackTrace();
		}
		ContentValues joinValues = new ContentValues();
		joinValues.put("article_id", articleId);
		joinValues.put("section_id", sectionId);
		joinValues.put("priority", priority);
		db.insert(MobStacSQLiteOpenHelper.SECTION_ARTICLE_JOIN_TABLE, null,
				joinValues);
	}

	/**
	 * This is a utility function to pre-load HomePage sections from the splash
	 * screen.
	 * 
	 * @return status of the sync operation.
	 */
	public boolean syncHomePage(ContentProviderClient provider) {
		Uri.Builder uri = MobStacProvider.getContentUri(
				mContext.getResources()
				.getString(
						MobStacProvider.getResourseIdByName(
								mContext.getPackageName(), "string",
								"provider"))).buildUpon();

		uri.appendEncodedPath(mobsiteId + "/sections/0/");
		uri.appendQueryParameter("forceDB", "true");
		uri.appendQueryParameter("limit", "1");
		boolean success = true;
		try {
			Cursor sections = provider.query(uri.build(), null, "enabled=?",
					new String[] { "1" }, null);
			if(sections != null && sections.getCount() > 0)
			{
				sections.moveToFirst();
				while (sections.moveToNext()) {
					if (sections.getInt(sections.getColumnIndex("article_count")) > 0) {
						int sectionId = sections.getInt(sections
								.getColumnIndex("_id"));
						success = success && syncArticles(sectionId, false, "" + sections.getInt(sections.getColumnIndex("article_count")));
					}
				}
			}
			else
				success = false;
			sections.close();

		} catch (RemoteException e) {
			e.printStackTrace();
		}
		return success;
	}

	JSONArray syncComments(String article_id) {
		String api_url = mobsiteId + "/contentitems/"
				+ article_id + "/comments/";
		JSONObject comments = callAPI(api_url, null, "GET");
		if (comments != null) {
			JSONArray comment_list;
			try {
				comment_list = comments.getJSONArray("comments");
				return comment_list;
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return null;
	}

	/**
	 * Provides a utility to start push notification service
	 * @param path
	 * 			  This should contain the path to the API after
	 *            https://api.mobstac.com/api/1.0/mobsites/
	 * @param params
	 * 			  Pass query parameters relevant to the method being used.
	 * @return
	 * 			Returns JSONObject with relevant ID from SNS.
	 */
	public JSONObject syncSNS(String path, Map<String, String> params, String method)
	{
		String complete_path = mobsiteId + path; 
		JSONObject snsCredentials = callAPI(complete_path, params, method);
		return snsCredentials;
	}

	/**
	 * Provides a utility function to communicate with MobStac's API.
	 * 
	 * @param path
	 *            This should contain the path to the API after
	 *            https://api.mobstac.com/api/1.0/mobsites/
	 * @param params
	 *            All the query parameters should be a part of this map and not
	 *            path
	 */


	private JSONObject callAPI(String path, Map<String, String> params, String request_method)
	{
		Resources res = mContext.getResources();
		String url = "/api/1.0" + "/mobsites/" + path;
		Uri.Builder apiUri = Uri.parse(url).buildUpon();
		if(request_method.equalsIgnoreCase("GET"))
		{
			if (params != null) {
				params.put("api_key", apiKey);
				SortedSet<String> keys = new TreeSet<String>(params.keySet());
				for (String key : keys) {
					apiUri.appendQueryParameter(key, params.get(key));
				}
			} else {
				apiUri.appendQueryParameter("api_key", apiKey);
			}
			return downloadData(apiUri.build().toString());
		}
		else
		{
			apiUri.appendQueryParameter("api_key",apiKey);
			String changed_url = apiUri.build().toString();
			byte[] digest;
			try {
				digest = mac.doFinal(URLDecoder.decode(changed_url, "utf-8")
						.getBytes());
				boolean isDebug = (0 != (mContext.getApplicationInfo().flags &= ApplicationInfo.FLAG_DEBUGGABLE));
				String retVal = bytesToHex(digest).toLowerCase(Locale.US);
				if (isDebug)
					changed_url = "https://api.qa.mobstac.com"
							+ changed_url.concat("&signature=" + retVal);
				else
					changed_url = "https://api.mobstac.com"
							+ changed_url.concat("&signature=" + retVal);
				HttpPost httpPost = new HttpPost(changed_url);
				HttpClient httpClient = new DefaultHttpClient();
				HttpResponse response = null;
				List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(
						2);
				SortedSet<String> keys = new TreeSet<String>(params.keySet());
				for(String key:keys)
				{
					nameValuePairs.add(new BasicNameValuePair(key, params.get(key)));
				}
				httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
				response = httpClient.execute(httpPost);
				StringBuilder sb = new StringBuilder();
				try {
					BufferedReader reader = 
							new BufferedReader(new InputStreamReader(response.getEntity().getContent()), 65728);
					String line = null;

					while ((line = reader.readLine()) != null) {
						sb.append(line);
					}
					JSONObject jsonObj = new JSONObject(sb.toString());
					httpClient.getConnectionManager().shutdown();
					return jsonObj;
				}
				catch (IOException e) { e.printStackTrace(); }
				catch (Exception e) { e.printStackTrace(); }
			}
			catch (IllegalStateException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ClientProtocolException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}return null;
		}
	}

}
