package com.mobstac.api;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.ContentProvider;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.util.Log;

import com.mobstac.api.database.MobStacSQLiteOpenHelper;
import com.mobstac.api.database.models.Article;
import com.mobstac.api.database.models.Media;
import com.mobstac.api.database.models.Mobsite;
import com.mobstac.api.database.models.RelatedArticles;
import com.mobstac.api.database.models.RelatedTopics;
import com.mobstac.api.database.models.Section;
import com.mobstac.api.database.models.SubSection;

/**
 * MobStacProvider is an abstract class that derives from
 * {@link ContentProvider} and provides an easy way to interact with the MobStac
 * API.
 * <p>
 * To use {@link MobStacProvider}, you'll have to override the
 * {@link MobStacProvider} with an empty body. This is done to allow multiple
 * apps using the same Provider on different platforms.
 * </p>
 * Currently, it's essential that before calling {@link MobStacProvider}, you
 * should call {@link SyncDownloader#syncMobsite()}.<br/>
 * <p>
 * To use {@link MobStacProvider}, you should have the following constants
 * defined in your strings.xml: <br/>
 * </p>
 * <p>
 * {@literal mobsite_id}(integer) - This is same as the mobsite_id passed to
 * ContentProvider. <br/>
 * {@literal provider} - This is the string used to determine URI for
 * ContentProvider. <br/>
 * {@literal api_key} - The api_key as provided to you to communicate with
 * MobStac API <br/>
 * {@literal secret_key} - The secret_key as provided to you to communicate with
 * MobStac API.
 * <p>
 * To use MobStacProvider you need to call {@link ContentResolver} with the
 * {@link Uri} registered by MobStacProvider. The list of available {@link Uri}
 * is as follows: <br/>
 * </p>
 * 
 * content://&lt;provider&gt;/mobsites/&lt;mobsite_id&gt;/ <br/>
 * 
 * content://&lt;provider&gt;/mobsites/&lt;mobsite_id&gt;/sections/ <br/>
 * 
 * content://&lt;provider&gt;/mobsites/&lt;mobsite_id&gt;/sections/&lt;
 * section_id&gt;/ <br/>
 * 
 * content://&lt;provider&gt;/mobsites/&lt;mobsite_id&gt;/sections/&lt;
 * section_id&gt;/settings/ <br/>
 * 
 * content://&lt;provider&gt;/mobsites/&lt;mobsite_id&gt;/sections/&lt;
 * section_id&gt;/subsections/ <br/>
 * 
 * content://&lt;provider&gt;/mobsites/&lt;mobsite_id&gt;/sections/&lt;
 * section_id&gt;/contents/ <br/>
 * 
 * content://&lt;provider&gt;/mobsites/&lt;mobsite_id&gt;/sections/&lt;
 * section_id&gt;/contents/&lt;content_id&gt;/ <br/>
 * 
 * content://&lt;provider&gt;/mobsites/&lt;mobsite_id&gt;/subsections/ <br/>
 * 
 * content://&lt;provider&gt;/mobsites/&lt;mobsite_id&gt;/contents/&lt;
 * content_id&gt;/ <br/>
 * 
 * content://&lt;provider&gt;/mobsites/&lt;mobsite_id&gt;/contents/url/ <br/>
 * 
 * content://&lt;provider&gt;/mobsites/&lt;mobsite_id&gt;/contents/ <br/>
 * 
 * content://&lt;provider&gt;/mobsites/&lt;mobsite_id&gt;/contents/&lt;
 * content_id&gt;/media/ <br/>
 * 
 * content://&lt;provider&gt;/mobsites/&lt;mobsite_id&gt;/contents/&lt;
 * content_id&gt;/comments/ <br/>
 * 
 * content://&lt;provider&gt;/mobsites/&lt;mobsite_id&gt;/contents/&lt;
 * content_id&gt;/articles/ <br/>
 * 
 * content://&lt;provider&gt;/mobsites/&lt;mobsite_id&gt;/contents/&lt;
 * content_id&gt;/topics/ <br/>
 * 
 * content://&lt;provider&gt;/mobsites/&lt;mobsite_id&gt;/push/register/ <br/>
 * 
 * content://&lt;provider&gt;/mobsites/&lt;mobsite_id&gt;/push/subscribe/ <br/>
 * 
 * content://&lt;provider&gt;/mobsites/&lt;mobsite_id&gt;/push/unsubscribe/ <br/>
 * 
 * Not all of these are available with different methods. Please look at the
 * documentation for individual method to know which {@link Uri} is supported.
 * 
 * <br/>
 */
public abstract class MobStacProvider extends ContentProvider {

	private static final int MOBSITE = 1;
	private static final int SECTION = 2;
	private static final int SECTION_ID = 3;
	private static final int SECTION_SETTINGS = 4;
	private static final int SUB_SECTION = 5;
	private static final int SUB_SECTIONS = 6;
	private static final int ARTICLE = 7;
	private static final int SECTION_ARTICLE_ID = 8;
	private static final int ARTICLE_ID = 9;
	private static final int ARTICLE_MEDIA = 10;
	private static final int MEDIA = 11;
	private static final int STARREDARTICLE = 12;
	private static final int COMMENTS = 13;
	private static final int SUB_SECTION_DATA = 14;
	private static final int RELATED_ARTICLE = 15;
	private static final int RELATED_TOPIC = 16;
	private static final int RELATED_ARTICLE_URL = 17;
	private static final int RELATED_TOPIC_URL = 18;
	private static final int ARTICLE_URL = 19;
	private static final int SAVE_ARTICLE_RELATED = 20;
	private static final int PUSH_REGISTER = 21;
	private static final int PUSH_SUBSCRIBE = 22;
	private static final int PUSH_UNSUBSCRIBE = 23;
	private static final int SNS_REGISTER_DH = 24;
	private static final int SNS_SUBSCRIBE_DH = 25;
	private static final int SNS_UNSUBSCRIBE_DH = 26;
	

	public static final String BASE_PATH = "mobsites";

	public static Uri getContentUri(String authority) {
		return Uri.parse("content://" + authority + "/" + BASE_PATH);
	}

	private static UriMatcher sURIMatcher;

	private static Object lock = new Object();

	public final String getAuthority() {
		return getContext().getResources().getString(
				MobStacProvider.getResourseIdByName(getContext()
						.getPackageName(), "string", "provider"));
	}

	@SuppressWarnings("rawtypes")
	static int getResourseIdByName(String packageName, String className,
			String name) {
		Class r = null;
		int id = 0;
		try {
			r = Class.forName(packageName + ".R");

			Class[] classes = r.getClasses();
			Class desireClass = null;

			for (int i = 0; i < classes.length; i++) {
				if (classes[i].getName().split("\\$")[1].equals(className)) {
					desireClass = classes[i];

					break;
				}
			}

			if (desireClass != null)
				id = desireClass.getField(name).getInt(desireClass);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (NoSuchFieldException e) {
			e.printStackTrace();
		}

		return id;

	}
	
	/**
	 * This method initializes MobStacProvider. This should not be called
	 * explicitly and is only meant to be called from the base class on
	 * initialization.
	 * 
	 * @see ContentProvider#onCreate()
	 * 
	 * @return true/false depending on successful initialization
	 */
	@Override
	public boolean onCreate() {
		synchronized (lock) {
			if (sURIMatcher == null) {
				sURIMatcher = new UriMatcher(UriMatcher.NO_MATCH);
				sURIMatcher.addURI(getAuthority(), BASE_PATH + "/#/", MOBSITE);
				sURIMatcher.addURI(getAuthority(), BASE_PATH + "/#/sections/",
						SECTION);
				sURIMatcher.addURI(getAuthority(),
						BASE_PATH + "/#/sections/#/", SECTION_ID);
				sURIMatcher.addURI(getAuthority(), BASE_PATH
						+ "/#/sections/#/settings/", SECTION_SETTINGS);
				sURIMatcher.addURI(getAuthority(), BASE_PATH
						+ "/#/sections/#/subsections/", SUB_SECTION);
				sURIMatcher.addURI(getAuthority(), BASE_PATH
						+ "/#/sections/#/subsectiondata/", SUB_SECTION_DATA);
				sURIMatcher.addURI(getAuthority(), BASE_PATH
						+ "/#/sections/#/contents/", ARTICLE);
				sURIMatcher.addURI(getAuthority(), BASE_PATH
						+ "/#/sections/#/contents/#/", SECTION_ARTICLE_ID);
				sURIMatcher.addURI(getAuthority(), BASE_PATH
						+ "/#/subsections/", SUB_SECTIONS);
				sURIMatcher.addURI(getAuthority(),
						BASE_PATH + "/#/contents/#/", ARTICLE_ID);
				sURIMatcher.addURI(getAuthority(), BASE_PATH
						+ "/#/contents/url/", ARTICLE_URL);
				sURIMatcher.addURI(getAuthority(), BASE_PATH + "/#/contents/",
						STARREDARTICLE);
				sURIMatcher.addURI(getAuthority(), BASE_PATH
						+ "/#/contents/#/media/", ARTICLE_MEDIA);
				sURIMatcher.addURI(getAuthority(), BASE_PATH + "/#/media/#/",
						MEDIA);
				sURIMatcher.addURI(getAuthority(), BASE_PATH
						+ "/#/contents/#/comments/", COMMENTS);
				sURIMatcher.addURI(getAuthority(), BASE_PATH
						+ "/#/contents/#/articles/", RELATED_ARTICLE);
				sURIMatcher.addURI(getAuthority(), BASE_PATH
						+ "/#/contents/#/topics/", RELATED_TOPIC);
				sURIMatcher.addURI(getAuthority(), BASE_PATH
						+ "/#/contents/articles/", RELATED_ARTICLE_URL);
				sURIMatcher.addURI(getAuthority(), BASE_PATH
						+ "/#/contents/#/savearticle/", SAVE_ARTICLE_RELATED);
				sURIMatcher.addURI(getAuthority(), BASE_PATH
						+ "/#/push/register/", PUSH_REGISTER);
				sURIMatcher.addURI(getAuthority(), BASE_PATH
						+ "/#/push/subscribe/", PUSH_SUBSCRIBE);
				sURIMatcher.addURI(getAuthority(), BASE_PATH
						+ "/#/push/unsubscribe/", PUSH_UNSUBSCRIBE);
				sURIMatcher.addURI(getAuthority(), BASE_PATH
						+ "/#/sns/register/", SNS_REGISTER_DH);
				sURIMatcher.addURI(getAuthority(), BASE_PATH
						+ "/#/sns/subscribe/", SNS_SUBSCRIBE_DH);
				sURIMatcher.addURI(getAuthority(), BASE_PATH
						+ "/#/sns/unsubscribe/", SNS_UNSUBSCRIBE_DH);
			}
		}
		return true;
	}

	/**
	 * A utility function meant to check that the columns requested match what
	 * we can provide.
	 * 
	 * @param projection
	 *            expected columns in result set
	 * @param available
	 *            available columns in result set
	 */
	private void checkColumns(String[] projection, String[] available) {
		if (projection != null) {
			HashSet<String> requestedColumns = new HashSet<String>(
					Arrays.asList(projection));
			HashSet<String> availableColumns = new HashSet<String>(
					Arrays.asList(available));
			// Check if all columns which are requested are available
			if (!availableColumns.containsAll(requestedColumns)) {
				throw new IllegalArgumentException(
						"Unknown columns in projection");
			}
		}
	}

	/**
	 * 
	 * This is called to delete any particular item based on a {@link Uri}. This
	 * is not supported right now.
	 * 
	 */
	@Override
	public int delete(Uri uri, String selection, String[] selectionArgs) {
		MobStacSQLiteOpenHelper database = MobStacSQLiteOpenHelper
				.getInstance(getContext());
		SQLiteDatabase db = database.getWritableDatabase();
		switch (sURIMatcher.match(uri)) {
			case SUB_SECTION: {
				return db.delete(SubSection.getTableName(), selection, selectionArgs);
			}
			case SECTION_ARTICLE_ID: {
				String article_id = uri.getLastPathSegment();
				String section_id = uri.getPathSegments().get(3);
				return db.delete(MobStacSQLiteOpenHelper.SECTION_ARTICLE_JOIN_TABLE,
						"article_id=? AND section_id=?", new String[] { article_id, section_id });
			}
//			case SAVE_ARTICLE_RELATED:
//			{
//				return db.delete(Article.getTableName(), selection, selectionArgs);
//			}
		}
		return 0;

	}

	/**
	 * This method returns the type of data returned based on {@link Uri}. This
	 * is not supported right now.
	 * 
	 * @param uri {@link Uri} to get the type.
	 * 
	 * @return string which denotes the type of data for {@link Uri}
	 */
	@Override
	public String getType(Uri uri) {
		switch (sURIMatcher.match(uri)) {
			case MOBSITE:
				return "com.mobstac.api.database.models.Mobsite";
			case SECTION:
				return "com.mobstac.api.database.models.Section";
			case ARTICLE:
				return "com.mobstac.api.database.models.Article";
			case ARTICLE_MEDIA:
				return "com.mobstac.api.database.models.Media";
			default:
				return null;
		}
	}

	/**
	 * This method provides mechanism to insert data into the
	 * {@link MobStacProvider}. Following {@link Uri}s are supported:
	 * 
	 * content://&lt;provider&gt;/mobsites/&lt;mobsite_id&gt;/<br/>
	 * 
	 * content://&lt;provider&gt;/mobsites/&lt;mobsite_id&gt;/sections/<br/>
	 * 
	 * content://&lt;provider&gt;/mobsites/&lt;mobsite_id&gt;/sections/&lt;section_id&gt;/ <br/>
	 * 
	 * content://&lt;provider&gt;/mobsites/&lt;mobsite_id&gt;/sections/&lt;section_id&gt;/subsections/ <br/>
	 * 
	 * content://&lt;provider&gt;/mobsites/&lt;mobsite_id&gt;/sections/&lt;section_id&gt;/contents/ <br/>
	 * 
	 * content://&lt;provider&gt;/mobsites/&lt;mobsite_id&gt;/sections/&lt;section_id&gt;/contents/&lt;content_id&gt;/ <br/>
	 * 
	 * content://&lt;provider&gt;/mobsites/&lt;mobsite_id&gt;/contents/&lt;content_id&gt;/media/ <br/>
	 * 
	 * content://&lt;provider&gt;/mobsites/&lt;mobsite_id&gt;/contents/&lt;content_id&gt;/comments/ <br/>
	 * 
	 * content://&lt;provider&gt;/mobsites/&lt;mobsite_id&gt;/contents/&lt;content_id&gt;/articles/ <br/>
	 * 
	 * content://&lt;provider&gt;/mobsites/&lt;mobsite_id&gt;/contents/&lt;content_id&gt;/topics/ <br/>
	 * 
	 * content://&lt;provider&gt;/mobsites/&lt;mobsite_id&gt;/contents/&lt;content_id&gt;/savearticle/ <br/>
	 * 
	 */
	@Override
	public Uri insert(Uri uri, ContentValues values) {
		MobStacSQLiteOpenHelper database = MobStacSQLiteOpenHelper
				.getInstance(getContext());
		SQLiteDatabase db = database.getWritableDatabase();
		switch (sURIMatcher.match(uri)) {
			case MOBSITE: {
				db.insert(Mobsite.getTableName(), null, values);
				getContext().getContentResolver().notifyChange(uri, null);
				return uri;
			}
			case SECTION_ID:
			case SECTION: {
				db.insert(Section.getTableName(), null, values);
				getContext().getContentResolver().notifyChange(uri, null);
				return Uri.withAppendedPath(uri, values.getAsString("_id")
						+ "/");
			}
			case SUB_SECTION: {
				db.insert(SubSection.getTableName(), null, values);
				return uri;
			}
			case ARTICLE: {
				String article_id = values.getAsString("_id");
				int priority = values.getAsInteger("priority");
				values.remove("priority");
				try {
					db.insert(Article.getTableName(), null, values);
				} catch (SQLiteConstraintException e) {
					e.printStackTrace();
				}
				String section_id = uri.getPathSegments().get(3);
				ContentValues joinValues = new ContentValues();
				joinValues.put("article_id", article_id);
				joinValues.put("section_id", section_id);
				joinValues.put("priority", priority);
				db.insert(MobStacSQLiteOpenHelper.SECTION_ARTICLE_JOIN_TABLE,
						null, joinValues);
				getContext().getContentResolver().notifyChange(uri, null);
				return Uri.withAppendedPath(uri, article_id + "/");
			}
			case ARTICLE_MEDIA: {
				String article_id = uri.getPathSegments().get(3);
				values.put("article_id", article_id);
				try {
					db.insert(Media.getTableName(), null, values);
				} catch (SQLiteConstraintException e) {
					e.printStackTrace();
				}
				getContext().getContentResolver().notifyChange(uri, null);
				return Uri.withAppendedPath(uri, values.getAsString("_id")
						+ "/");
			}
			case RELATED_ARTICLE: {
				String article_id = uri.getPathSegments().get(3);
				values.put("content_id", article_id);
				try {
					db.insert(RelatedArticles.getTableName(), null, values);
				} catch (SQLiteConstraintException e) {
					e.printStackTrace();
				}
				getContext().getContentResolver().notifyChange(uri, null);
				return uri;
			}
			case RELATED_TOPIC: {
				String article_id = uri.getPathSegments().get(3);
				values.put("content_id", article_id);
				try {
					db.insert(RelatedTopics.getTableName(), null, values);
				} catch (SQLiteConstraintException e) {
					e.printStackTrace();
				}
				getContext().getContentResolver().notifyChange(uri, null);
				return uri;
			}
			case SECTION_ARTICLE_ID: {
				String article_id = uri.getLastPathSegment();
				String section_id = uri.getPathSegments().get(3);
				ContentValues joinValues = new ContentValues();
				joinValues.put("article_id", article_id);
				joinValues.put("section_id", section_id);
				joinValues.put("priority", values.getAsInteger("priority"));
				db.insert(MobStacSQLiteOpenHelper.SECTION_ARTICLE_JOIN_TABLE,
						null, joinValues);
				getContext().getContentResolver().notifyChange(uri, null);
				return uri;
			}
			case COMMENTS: {
				// TODO comments not implemented
				return null;
			}
			case SAVE_ARTICLE_RELATED:
			{
				String article_id = uri.getPathSegments().get(3);
				values.put("_id", article_id);
				try {
					db.insert(Article.getTableName(), null, values);
				} catch (SQLiteConstraintException e) {
					e.printStackTrace();
				}
				getContext().getContentResolver().notifyChange(uri, null);
				return uri;
			}

		}
		Log.v("Provider", "Insert done!");
		return null;
	}

	/**
	 * This method provides data for different queries on different {@link Uri}s. The 
	 * supported {@link Uri}s are:
	 * 
	 * content://&lt;provider&gt;/mobsites/&lt;mobsite_id&gt;/ <br/>
	 * 
	 * content://&lt;provider&gt;/mobsites/&lt;mobsite_id&gt;/sections/ <br/>
	 * 
	 * content://&lt;provider&gt;/mobsites/&lt;mobsite_id&gt;/sections/&lt;section_id&gt;/ <br/>
	 * 
	 * content://&lt;provider&gt;/mobsites/&lt;mobsite_id&gt;/sections/&lt;
	 * section_id&gt;/settings/ <br/>
	 * 
	 * content://&lt;provider&gt;/mobsites/&lt;mobsite_id&gt;/sections/&lt;
	 * section_id&gt;/subsections/ <br/>
	 * 
	 * content://&lt;provider&gt;/mobsites/&lt;mobsite_id&gt;/sections/&lt;
	 * section_id&gt;/contents/ <br/>
	 * 
	 * content://&lt;provider&gt;/mobsites/&lt;mobsite_id&gt;/sections/&lt;
	 * section_id&gt;/contents/&lt;content_id&gt;/ <br/>
	 * 
	 * content://&lt;provider&gt;/mobsites/&lt;mobsite_id&gt;/subsections/ <br/>
	 * 
	 * content://&lt;provider&gt;/mobsites/&lt;mobsite_id&gt;/contents/&lt;
	 * content_id&gt;/ <br/>
	 * 
	 * content://&lt;provider&gt;/mobsites/&lt;mobsite_id&gt;/contents/url/ <br/>
	 * 
	 * content://&lt;provider&gt;/mobsites/&lt;mobsite_id&gt;/contents/ <br/>
	 * 
	 * content://&lt;provider&gt;/mobsites/&lt;mobsite_id&gt;/contents/&lt;
	 * content_id&gt;/media/ <br/>
	 * 
	 * content://&lt;provider&gt;/mobsites/&lt;mobsite_id&gt;/contents/&lt;
	 * content_id&gt;/comments/ <br/>
	 * 
	 * content://&lt;provider&gt;/mobsites/&lt;mobsite_id&gt;/contents/&lt;
	 * content_id&gt;/articles/ <br/>
	 * 
	 * content://&lt;provider&gt;/mobsites/&lt;mobsite_id&gt;/contents/&lt;
	 * content_id&gt;/topics/ <br/>
	 * 
	 * Look at {@link ContentProvider} to understand the usage of different parameters.
	 */
	@Override
	public Cursor query(Uri uri, String[] projection, String selection,
			String[] selectionArgs, String sortOrder) {
		boolean isConnected = false;
		ConnectivityManager cm = (ConnectivityManager) getContext()
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo netInfo = cm.getActiveNetworkInfo();
		if (netInfo != null && netInfo.isConnected()
				&& uri.getQueryParameter("forceDB") == null) {
			isConnected = true;
		}
		SyncDownloader downloader = new SyncDownloader(getContext(), null);
		SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();
		MobStacSQLiteOpenHelper database = MobStacSQLiteOpenHelper
				.getInstance(getContext());
		SQLiteDatabase db = database.getReadableDatabase();
		String groupBy = null;
		String having = null;

		// Check if the caller has requested a column which does not exists
		switch (sURIMatcher.match(uri)) {
			case MOBSITE: {
				if (isConnected) {
					downloader.syncMobsite();
				}
				checkColumns(projection, Mobsite.getAllColumns());
				queryBuilder.setTables(Mobsite.getTableName());
				queryBuilder.appendWhere("_id=" + uri.getLastPathSegment());
				break;
			}
			case SECTION_ID: {
				queryBuilder.appendWhere("section_id="
						+ uri.getLastPathSegment());
				queryBuilder.setTables(SubSection.getTableName());
				Cursor cursor = queryBuilder.query(db,
						SubSection.getAllColumns(), "enabled=?",
						new String[] { "1" }, null, null, null, null);
				if (cursor!= null && cursor.getCount() > 0) {
					String[] orig = Section.getAllNamedColumns();
					projection = new String[orig.length
							+ SubSection.getAllSubSectionColumns().length]; 
					int i = 0;
					for (String data : orig) {
						projection[i] = data;
						i++;
					}

					for (String data : SubSection.getAllSubSectionColumns()) {
						projection[i] = data;
						i++;
					}
					queryBuilder = new SQLiteQueryBuilder();
					queryBuilder.setTables(SubSection.getTableName()
							+ " INNER JOIN " + Section.getTableName() + " ON "
							+ SubSection.getTableName() + "._id="
							+ Section.getTableName() + "._id");
					queryBuilder.appendWhere(SubSection.getTableName()
							+ ".section_id=" + uri.getLastPathSegment());
					queryBuilder.appendWhere(" AND enabled=1");
					sortOrder = SubSection.getTableName() + ".sequence";
				} else {
					selection = null;
					selectionArgs = null;
					queryBuilder = new SQLiteQueryBuilder();
					queryBuilder.appendWhere("_id=" + uri.getLastPathSegment());
					checkColumns(projection, Section.getAllColumns());
					queryBuilder.setTables(Section.getTableName());
				}
				cursor.close();
				break;
			}
			case SECTION_SETTINGS: {
				queryBuilder.appendWhere("_id=" + uri.getPathSegments().get(3));
				checkColumns(projection, Section.getAllColumns());
				queryBuilder.setTables(Section.getTableName());
				break;
			}
			case SECTION: {
				sortOrder = "sequence";
				checkColumns(projection, Section.getAllColumns());
				queryBuilder.setTables(Section.getTableName());
				Log.v("Provider", "Query Done");
				break;
			}
			case STARREDARTICLE: {
				String[] orig = Article.getAllArticleColumns();
				projection = new String[orig.length
						+ Media.getAllMediaColumns().length];
				int i = 0;
				for (String data : orig) {
					projection[i] = data;
					i++;
				}

				for (String data : Media.getAllMediaColumns()) {
					projection[i] = data;
					i++;
				}

				queryBuilder.setTables(Article.getTableName()
						+ " LEFT OUTER JOIN " + Media.getTableName() + " ON "
						+ Media.getTableName() + ".article_id="
						+ Article.getTableName() + "._id"
						+ " AND "+ Media.getTableName() + "._id ="
						+ "(SELECT MIN(" + Media.getTableName() + "._id) "
						+ "FROM "+ Media.getTableName() + " WHERE article_id="
						+ Article.getTableName() + "._id)");
				groupBy = Article.getTableName() + "._id";
				sortOrder = Media.getTableName() + ".sequence ASC";

				break;
			}
			case SUB_SECTIONS: {
				queryBuilder.setTables(SubSection.getTableName());
				break;
			}
			case SUB_SECTION: {
				String[] orig = Section.getAllNamedColumns();
				projection = new String[orig.length
						+ SubSection.getAllSubSectionColumns().length];
				int i = 0;
				for (String data : orig) {
					projection[i] = data;
					i++;
				}

				for (String data : SubSection.getAllSubSectionColumns()) {
					projection[i] = data;
					i++;
				}
				queryBuilder.setTables(SubSection.getTableName()
						+ " INNER JOIN " + Section.getTableName() + " ON "
						+ SubSection.getTableName() + "._id="
						+ Section.getTableName() + "._id");
				queryBuilder.appendWhere(SubSection.getTableName()
						+ ".section_id=" + uri.getPathSegments().get(3));
				queryBuilder.appendWhere(" AND " + SubSection.getTableName()
						+ ".show_title=1");
				sortOrder = SubSection.getTableName() + ".sequence ASC";
				break;
			}
			case SUB_SECTION_DATA: {
				String[] orig = SubSection.getAllSubSectionColumns();
				projection = new String[orig.length
						+ Section.getAllNamedColumns().length];
				int i = 0;
				for (String data : orig) {
					projection[i] = data;
					i++;
				}

				for (String data : Section.getAllNamedColumns()) {
					projection[i] = data;
					i++;
				}
				queryBuilder.appendWhere(SubSection.getTableName()
						+ ".section_id=" + uri.getPathSegments().get(3));
				sortOrder = SubSection.getTableName() + ".sequence";
				queryBuilder.setTables(SubSection.getTableName()
						+ " INNER JOIN " + Section.getTableName() + " ON "
						+ SubSection.getTableName() + "._id="
						+ Section.getTableName() + "._id");
				break;
			}
			case ARTICLE_ID: {
				queryBuilder.appendWhere("_id=" + uri.getLastPathSegment());
				checkColumns(projection, Article.getAllColumns());
				queryBuilder.setTables(Article.getTableName());
				break;
			}
			case ARTICLE_URL: {
				String[] orig = Article.getAllArticleColumns();
				projection = new String[orig.length
						+ Media.getAllMediaColumns().length];
				int i = 0;
				for (String data : orig) {
					projection[i] = data;
					i++;
				}

				for (String data : Media.getAllMediaColumns()) {
					projection[i] = data;
					i++;
				}
				queryBuilder.setTables(Article.getTableName()
						+ " LEFT OUTER JOIN " + Media.getTableName() + " ON "
						+ Media.getTableName() + ".article_id="
						+ Article.getTableName() + "._id");
				queryBuilder.appendWhere(Article.getTableName() + ".url LIKE '"
						+ uri.getQueryParameter("url") + "'");
				Cursor cursor = queryBuilder.query(db, projection, null, null,
						null, null, null);
				if (cursor.getCount() > 0) {
					return cursor;
				} else {
					return downloader.syncArticle(
							uri.getQueryParameter("title"),
							uri.getQueryParameter("url"));
				}
			}
			case SECTION_ARTICLE_ID: {
				queryBuilder.appendWhere("Article._id="
						+ uri.getLastPathSegment());
			}
			case ARTICLE: {
				if (isConnected) {
					int sectionId = Integer.parseInt(uri.getPathSegments().get(3));
					String count = "";
					if(uri.getQueryParameter("count")!=null)
						count = uri.getQueryParameter("count");
					else if(uri.getQueryParameter("limit")!= null)
						count = uri.getQueryParameter("limit");
					else
						count = ""+20;
					if (sectionId > 1) {
						downloader.syncArticles(sectionId,
								uri.getQueryParameter("forceLoad") != null, count);
					}
				}
				String[] orig = Article.getAllArticleColumns();
				projection = new String[orig.length
						+ Media.getAllMediaColumns().length];
				int i = 0;
				for (String data : orig) {
					projection[i] = data;
					i++;
				}

				for (String data : Media.getAllMediaColumns()) {
					projection[i] = data;
					i++;
				}
				if (selection != null && selection.contains("Section.name")) {
					queryBuilder.setTables(Article.getTableName()
									+ " INNER JOIN "
									+ MobStacSQLiteOpenHelper.SECTION_ARTICLE_JOIN_TABLE
									+ " ON "
									+ Article.getTableName()
									+ "._id="
									+ MobStacSQLiteOpenHelper.SECTION_ARTICLE_JOIN_TABLE
									+ ".article_id"
									+ " INNER JOIN "
									+ Section.getTableName()
									+ " ON "
									+ Section.getTableName()
									+ "._id="
									+ MobStacSQLiteOpenHelper.SECTION_ARTICLE_JOIN_TABLE
									+ ".section_id" + " LEFT OUTER JOIN "
									+ Media.getTableName() + " ON "
									+ Media.getTableName() + ".article_id="
									+ Article.getTableName() + "._id"
									+ " AND "+ Media.getTableName() + "._id ="
									+ "(SELECT MIN(" + Media.getTableName() + "._id) "
									+ "FROM "+ Media.getTableName() + " WHERE article_id="
									+ Article.getTableName() + "._id)");
				} else {
					String section_id = uri.getPathSegments().get(3);
					queryBuilder.setTables(Article.getTableName()
									+ " INNER JOIN "
									+ MobStacSQLiteOpenHelper.SECTION_ARTICLE_JOIN_TABLE
									+ " ON "
									+ Article.getTableName()
									+ "._id="
									+ MobStacSQLiteOpenHelper.SECTION_ARTICLE_JOIN_TABLE
									+ ".article_id AND "
									+ MobStacSQLiteOpenHelper.SECTION_ARTICLE_JOIN_TABLE
									+ ".section_id="
									+ section_id
									+ " INNER JOIN "
									+ Section.getTableName()
									+ " ON "
									+ Section.getTableName()
									+ "._id="
									+ MobStacSQLiteOpenHelper.SECTION_ARTICLE_JOIN_TABLE
									+ ".section_id" + " LEFT OUTER JOIN "
									+ Media.getTableName() + " ON "
									+ Media.getTableName() + ".article_id="
									+ Article.getTableName() + "._id"
									+ " AND "+ Media.getTableName() + "._id ="
									+ "(SELECT MIN(" + Media.getTableName() + "._id) "
									+ "FROM "+ Media.getTableName() + " WHERE article_id="
									+ Article.getTableName() + "._id)"
									);
				}
				groupBy = Article.getTableName() + "._id";
				sortOrder = Section.getTableName()
						+ ".sequence ASC, " + MobStacSQLiteOpenHelper.SECTION_ARTICLE_JOIN_TABLE
						+ ".priority DESC";
				break;
			}
			case ARTICLE_MEDIA: {
				String article_id = uri.getPathSegments().get(3);
				projection = new String[Media.getAllMediaColumns().length];
				int i = 0;
				for (String data : Media.getAllMediaColumns()) {
					projection[i] = data;
					i++;
				}
				checkColumns(projection, Media.getAllMediaColumns());
				queryBuilder.setTables(Article.getTableName() + " INNER JOIN "
						+ Media.getTableName() + " ON "
						+ Article.getTableName() + "._id="
						+ Media.getTableName() + ".article_id");
				queryBuilder.appendWhere(Article.getTableName() + "._id="
						+ article_id);
				sortOrder = Media.getTableName() + ".sequence ASC";
				break;
			}
			case RELATED_ARTICLE: {
				projection = RelatedArticles.getAllColumns();
				queryBuilder.appendWhere(RelatedArticles.getTableName()
						+ ".content_id=" + uri.getPathSegments().get(3));
				queryBuilder.setTables(RelatedArticles.getTableName());
				break;
			}
			case RELATED_TOPIC: {
				projection = RelatedTopics.getAllColumns();
				queryBuilder.appendWhere(RelatedTopics.getTableName()
						+ ".content_id=" + uri.getPathSegments().get(3));
				queryBuilder.setTables(RelatedTopics.getTableName());
				break;
			}
			case RELATED_ARTICLE_URL: {
				checkColumns(projection, RelatedArticles.getAllColumns());
				queryBuilder.setTables(RelatedArticles.getTableName());
				break;
			}
			case RELATED_TOPIC_URL: {
				checkColumns(projection, RelatedTopics.getAllColumns());
				queryBuilder.setTables(RelatedTopics.getTableName());
				break;
			}
			case MEDIA: {
				String media_id = uri.getLastPathSegment();
				queryBuilder.appendWhere("_id=" + media_id);
				queryBuilder.setTables(Media.getTableName());
				sortOrder = Media.getTableName() + ".sequence ASC";
				break;
			}
			case COMMENTS: {
				String article_id = uri.getPathSegments().get(3);
				JSONArray comments = downloader.syncComments(article_id);
				if (comments != null && comments.length() > 0) {
					try {
						String[] columnNames = new String[comments
								.getJSONObject(0).length()];
						Iterator<String> keys;
						keys = comments.getJSONObject(0).keys();
						for (int i = 0; keys.hasNext(); i++) {
							String key = keys.next();
							if (key.equals("id")) {
								columnNames[i] = "_id";
							} else {
								columnNames[i] = key;
							}
						}
						MatrixCursor cursor = new MatrixCursor(columnNames);
						for (int i = 0; i < comments.length(); i++) {
							keys = comments.getJSONObject(i).keys();
							Object[] values = new Object[comments
									.getJSONObject(i).length()];
							for (int j = 0; j < columnNames.length; j++) {
								if (columnNames[j].equals("_id")) {
									values[j] = comments.getJSONObject(i).get(
											"id");
								} else {
									values[j] = comments.getJSONObject(i).get(
											columnNames[j]);
								}
							}
							cursor.addRow(values);
						}
						return cursor;
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				return null;
			}
			case PUSH_REGISTER: {
				Cursor mobsite_cursor = db.query(Mobsite.getTableName(), Mobsite.getAllColumns(), 
						"pushNotification=?", new String[] {"1"}, null, null, null);
				if (mobsite_cursor != null) {
					if(mobsite_cursor.getCount() > 0){
						String path = "/push/register/";
						Map <String, String> params = new HashMap<String, String>(); 
						params.put("device_token", uri.getQueryParameter("registration_id"));
						params.put("device_platform", "GCM");
						JSONObject credentials = downloader.syncSNS(path, params, "POST");
						if(credentials != null){
							MatrixCursor sns_credentials = new MatrixCursor(new String[] {"_id", "success", "reason", "device_endpoint_arn"});
							try {
								MatrixCursor.RowBuilder row = sns_credentials.newRow();
								row.add(1);
								row.add (credentials.getBoolean("success"));
								if (credentials.getBoolean("success") == false)
									row.add (credentials.getString("reason"));
								else
									row.add ("");
								if(credentials.getString("endpoint_arn") != null)
									row.add(credentials.getString("endpoint_arn"));
								else
									row.add("");
								mobsite_cursor.close();
								return sns_credentials;
							} catch (JSONException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
								mobsite_cursor.close();
								return null;
							}
						}
					}
					mobsite_cursor.close();
				}
				else
					Log.v("Push Notification","The app is not registered for Push notification.");
				return null;
			}
			case PUSH_SUBSCRIBE: {
				int section_id = -1;
				Cursor mobsite_cursor = db.query(Mobsite.getTableName(), Mobsite.getAllColumns(), 
						"pushNotification=?", new String[] {"1"}, null, null, null);
				if (mobsite_cursor != null) {
					if(mobsite_cursor.getCount() > 0){
						Cursor section_cursor = db.query(Section.getTableName(), Section.getAllColumns(), 
								"pushNotification=?", new String[] {"1"}, null, null, null);
						if (section_cursor != null){
							if(section_cursor.getCount() > 0){
								section_cursor.moveToFirst();
								if (section_cursor.getLong(section_cursor.getColumnIndex("_id")) >=0)
									section_id = (int) section_cursor.getLong(section_cursor.getColumnIndex("_id"));
							}
							section_cursor.close();
						}
					}
					mobsite_cursor.close();
				}
				if (section_id != -1) {
					String path = "/sections/" + section_id + "/push/subscribe/";
					Map <String, String> params = new HashMap<String, String>();
					params.put("endpoint_arn", uri.getQueryParameter("device_endpoint_arn"));
					JSONObject credentials = downloader.syncSNS(path, params, "POST");
					if(credentials != null){
						MatrixCursor sns_credentials = new MatrixCursor(new String[] {"_id", "success", "reason", "subscription_id"});
						try {
							MatrixCursor.RowBuilder row = sns_credentials.newRow();
							row.add(1);
							row.add (credentials.getBoolean("success"));
							if (credentials.getBoolean("success") == false)
								row.add (credentials.getString("reason"));
							else
								row.add ("");
							if(credentials.getString("subscription_id") != null)
								row.add(credentials.getString("subscription_id"));
							else
								row.add("");
							return sns_credentials;
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							return null;
						}
					}
				}
				else
					Log.v("Push Notification", "Push notification not enabled for any section.");
				return null;
			}
			case PUSH_UNSUBSCRIBE: {
				int section_id = -1;
				Cursor mobsite_cursor = db.query(Mobsite.getTableName(), Mobsite.getAllColumns(), 
						"pushNotification=?", new String[] {"1"}, null, null, null);
				if (mobsite_cursor != null && mobsite_cursor.getCount() > 0) {
					Cursor section_cursor = db.query(Section.getTableName(), Section.getAllColumns(), 
							"pushNotification=?", new String[] {"1"}, null, null, null);
					if (section_cursor != null)
					{
						if(section_cursor.getCount() > 0){
							section_cursor.moveToFirst();
							if (section_cursor.getLong(section_cursor.getColumnIndex("_id")) >=0)
								section_id = (int) section_cursor.getLong(section_cursor.getColumnIndex("_id"));
						}
						section_cursor.close();
					}
				}
				if (section_id != -1) {
					String path = "/sections/" + section_id +"/push/unsubscribe/";
					Map <String, String> params = new HashMap<String, String>();
					params.put("subscription_id", uri.getQueryParameter("subscription_id"));
					JSONObject credentials = downloader.syncSNS(path, params, "POST");
					if(credentials != null){
						MatrixCursor sns_credentials = new MatrixCursor(new String[] {"_id", "success", "reason"});
						try {
							MatrixCursor.RowBuilder row = sns_credentials.newRow();
							row.add(1);
							row.add(credentials.getBoolean("success"));
							if (credentials.getBoolean("success") == false)
								row.add (credentials.getString("reason"));
							else
								row.add ("");
							return sns_credentials;
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							return null;
						}
					}
				}
				else
					Log.v("Push Notification", "Push notification not enabled for any section.");
				return null;
			}
			
			//Endpoint for DeccanHerald push notification registration
			case SNS_REGISTER_DH: {
				String path = "/sns/register/";
				Map <String, String> params = new HashMap<String, String>();
				params.put("device_token", uri.getQueryParameter("registration_id"));
				params.put("device_platform", "GCM");
				JSONObject credentials = downloader.syncSNS(path, params, "POST");
				if(credentials != null){
					MatrixCursor sns_credentials = new MatrixCursor(new String[] {"_id", "device_endpoint_arn"});
					try {
						MatrixCursor.RowBuilder row = sns_credentials.newRow();
						row.add(1);
						if(credentials.getString("endpoint_arn") != null)
							row.add(credentials.getString("endpoint_arn"));
						else
							row.add("");
						return sns_credentials;
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				else
					return null;
			}
			
			//Endpoint for DeccanHerald push notification subscription
			case SNS_SUBSCRIBE_DH: {
				String path = "/sns/subscribe/";
				Map <String, String> params = new HashMap<String, String>();
				params.put("endpoint_arn", uri.getQueryParameter("DeviceEndpointArn"));
				params.put("topic", uri.getQueryParameter("topicName"));
				JSONObject credentials = downloader.syncSNS(path, params, "POST");
				if(credentials != null){
					MatrixCursor sns_credentials = new MatrixCursor(new String[] {"_id", "subscription_id"});
					try {
						MatrixCursor.RowBuilder row = sns_credentials.newRow();
						row.add(1);
						if(credentials.getString("subscription_id") != null)
							row.add(credentials.getString("subscription_id"));
						else
							row.add("");
						return sns_credentials;
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				else
					return null;
			}
			
			//Endpoint for DeccanHerald push notification unsubscription
			case SNS_UNSUBSCRIBE_DH: {
				String path = "/sns/unsubscribe/";
				Map <String, String> params = new HashMap<String, String>();
				params.put("subscription_id", uri.getQueryParameter("subscription_id"));
				JSONObject credentials = downloader.syncSNS(path, params, "POST");
				if(credentials != null){
					MatrixCursor sns_credentials = new MatrixCursor(new String[] {"_id", "success"});
					try {
						MatrixCursor.RowBuilder row = sns_credentials.newRow();
						row.add(1);
						row.add(credentials.getString("success"));
						return sns_credentials;
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				else
					return null;
			}
			default: {
				Log.v("Uri no match: ", uri.toString());
			}

		}

		String limitString = "";
		if (uri.getQueryParameter("start") != null
				&& uri.getQueryParameter("limit") == null) {
			throw new NullPointerException("limit not found exception");
		}
		if (uri.getQueryParameter("limit") != null) {
			if (uri.getQueryParameter("start") != null) {
				limitString = uri.getQueryParameter("start") + ",";
			}
			limitString = limitString + uri.getQueryParameter("limit");
		}
		Cursor cursor = queryBuilder.query(db, projection, selection,
				selectionArgs, groupBy, having, sortOrder, limitString);

		if (uri.getQueryParameter("setNotification") != null) {
			cursor.setNotificationUri(getContext().getContentResolver(), uri);
		}
		return cursor;
	}

	/**
	 * This method provides a way to update the data in different {@link Uri}s.
	 * Following {@link Uri}s are supported:
	 * <br/>
	 * content://&lt;provider&gt;/mobsites/&lt;mobsite_id&gt;/sections/ <br/>
	 * 
	 * content://&lt;provider&gt;/mobsites/&lt;mobsite_id&gt;/sections/&lt;section_id&gt;/ <br/>
	 * 
	 * content://&lt;provider&gt;/mobsites/&lt;mobsite_id&gt;/sections/&lt;
	 * section_id&gt;/settings/ <br/>
	 * 
	 * content://&lt;provider&gt;/mobsites/&lt;mobsite_id&gt;/sections/&lt;
	 * section_id&gt;/subsections/ <br/>
	 * 
	 * content://&lt;provider&gt;/mobsites/&lt;mobsite_id&gt;/sections/&lt;
	 * section_id&gt;/contents/&lt;content_id&gt;/ <br/>
	 * 
	 * content://&lt;provider&gt;/mobsites/&lt;mobsite_id&gt;/contents/&lt;
	 * content_id&gt;/ <br/>
	 */
	@Override
	public int update(Uri uri, ContentValues values, String selection,
			String[] selectionArgs) {
		MobStacSQLiteOpenHelper database = MobStacSQLiteOpenHelper
				.getInstance(getContext());
		SQLiteDatabase db = database.getWritableDatabase();
		switch (sURIMatcher.match(uri)) {
			case MOBSITE: {
				int affected = db.update(Mobsite.getTableName(), values,
						"_id=?", new String[] { uri.getLastPathSegment() });
				if (affected > 0)
					getContext().getContentResolver().notifyChange(uri, null);
				return affected;
			}
			case SECTION: {
				int affected = db.update(Section.getTableName(), values, null,
						null);
				if (affected > 0) {
					getContext().getContentResolver().notifyChange(uri, null);
				}
				return affected;
			}
			case SECTION_SETTINGS: {
				int affected = db.update(Section.getTableName(), values,
						"_id=?", new String[] { uri.getPathSegments().get(3) });
				if (affected > 0) {
					getContext().getContentResolver().notifyChange(uri, null);
					if (values.containsKey("selected")
							&& values.getAsString("selected").equals("1")) {
						Uri section_uri = getContentUri(getAuthority())
								.buildUpon()
								.appendEncodedPath(
										Integer.parseInt(uri.getPathSegments()
												.get(1)) + "/sections/")
								.build();
						getContext().getContentResolver().notifyChange(
								section_uri, null);
					}
				}
				Log.v("Provider", "Update Done");
				return affected;
			}
			case SUB_SECTION: {
				int affected = db.update(SubSection.getTableName(), values,
						selection, selectionArgs);
				Uri section_uri = getContentUri(getAuthority())
						.buildUpon()
						.appendEncodedPath(
								Integer.parseInt(uri.getPathSegments().get(1))
										+ "/sections/" + selectionArgs[0] + "/")
						.build();
				getContext().getContentResolver().notifyChange(uri, null);
				getContext().getContentResolver().notifyChange(section_uri,
						null);
				return affected;
			}
			case SECTION_ID: {
				int affected = db.update(Section.getTableName(), values,
						"_id=?", new String[] { uri.getLastPathSegment() });
				Log.v("Provider", "Update Done");
				return affected;
			}
			case ARTICLE_ID: {
				int priority = 0;
				if (values.containsKey("priority")) {
					priority = values.getAsInteger("priority");
					values.remove("priority");
				}
				int affected = db.update(Article.getTableName(), values,
						"_id=?", new String[] { uri.getLastPathSegment() });
				String section_id = uri.getQueryParameter("sectionId");
				if (section_id != null && section_id.length() > 0) {
					ContentValues joinValues = new ContentValues();
					joinValues.put("article_id", uri.getLastPathSegment());
					joinValues.put("section_id", section_id);
					if (priority != 0)
						joinValues.put("priority", priority);
					db.insert(
							MobStacSQLiteOpenHelper.SECTION_ARTICLE_JOIN_TABLE,
							null, joinValues);
				}

				return affected;
			}

			case SECTION_ARTICLE_ID: {
				String article_id = values.getAsString("_id");
				int priority = values.getAsInteger("priority");
				values.remove("priority");
				int affected = db.update(Article.getTableName(), values,
						"_id=?", new String[] { article_id });
				String section_id = uri.getPathSegments().get(3);
				ContentValues joinValues = new ContentValues();
				joinValues.put("article_id", article_id);
				joinValues.put("section_id", section_id);
				joinValues.put("priority", priority);
				db.insert(MobStacSQLiteOpenHelper.SECTION_ARTICLE_JOIN_TABLE,
						null, joinValues);
				getContext().getContentResolver().notifyChange(uri, null);
				return affected;
			}
		}
		return 0;

	}

}
