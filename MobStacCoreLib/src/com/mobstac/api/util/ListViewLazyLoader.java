package com.mobstac.api.util;

import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;

public class ListViewLazyLoader implements OnScrollListener {

        private int visibleThreshold = 5;
        private int currentPage = 1;
        private int previousTotal = 0;
        private boolean loading = true;
        private callback mFrag;

        public ListViewLazyLoader(callback f) {
        	this.mFrag = f;
        }
//        public ListViewLazyLoader(int visibleThreshold) {
//            this.visibleThreshold = visibleThreshold;
//        }

        public void onScroll(AbsListView view, int firstVisibleItem,
                int visibleItemCount, int totalItemCount) {
            if (loading) {
                if (totalItemCount > previousTotal) {
                    loading = false;
                    previousTotal = totalItemCount;
                    currentPage++;
                }
            }
            if (!loading && (totalItemCount - visibleItemCount) <= (firstVisibleItem + visibleThreshold)) {
                // I load the next page of gigs using a background task,
                // but you can call any function here.
            	loading = true;
                mFrag.refreshResults(currentPage);
            }
        }
        
        public void setLoading(boolean loading) {
        	this.loading = loading;
        }
        
        public void reset() {
            this.currentPage = 1;
            this.previousTotal = 0;
            this.loading = true;
        }
        
        public interface callback {
        	public void refreshResults(int page);
        }
        
        public int getCurrentPage() {
        	return currentPage;
        }
        
        public void onScrollStateChanged(AbsListView view, int scrollState) {
        }
    }