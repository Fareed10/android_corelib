package com.mobstac.api.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.util.Log;

public class MobStacProfiler {

	private static Map<String, MobStacProfiler> profilers;
	private List<Long> durations;
	private long startTime;
	private String key;

	static {
		profilers = new HashMap<String, MobStacProfiler>();
	}

	private MobStacProfiler(String key) {
		this.key = key;
		durations = new ArrayList<Long>();
	}

	public static synchronized MobStacProfiler getProfiler(String key) {
		MobStacProfiler profiler = profilers.get(key);
		if (profiler == null) {
			profiler = new MobStacProfiler(key);
			profilers.put(key, profiler);
		}
		return profiler;
	}

	public void startProfiling() {
		if (startTime == 0) {
			startTime = System.currentTimeMillis();
		}
	}

	public void stopProfiling() {
		if (startTime != 0) {
			durations.add(System.currentTimeMillis() - startTime);
			startTime = 0;
			Log.v("Profiling", "Key: " + key + " Time: " + durations.toString());
		}
	}
}
